
/* Раскоментируй меня!!! */
// function Human(age, name, surname) {    /* создаём обьект */
//     this.age = age
//     this.name = name
//     this.surname = surname
//     this.showSomethink = function () {      /* я функция Єкземпляра */
//         document.write("Привет , я функция Єкземпляра<br>")
//     }
// }
// Human.showSomethink2 = function () {     /*я метод функции-конструктора */
//     document.write("Привет , я метод функции-конструктора<br>")
// }  
// Human.prototype.showSomethink3 = function () {  /*а я вообще на прототипе */
//     document.write("Привет , а я вообще на прототипе<br>")
// }
// let humans = new Array()                               /* создаём 5 человек через наш конструктор */
// humans.push(new Human(15, "Питар1", "Питарасович"))
// humans.push(new Human(11, "Питар2", "Питарасович"))
// humans.push(new Human(134, "Питар3", "Питарасович"))
// humans.push(new Human(15, "Питар4", "Питарасович"))
// humans.push(new Human(187, "Питар5", "Питарасович"))
// function sort() {                                             /* сортируэм по возрасту */
//     let result = humans.sort(function (a, b) {
//         if (a.age > b.age) {
//             return 1;
//         }
//         if (a.age < b.age) {
//             return -1;
//         }
//         return 0;
//     });
//     console.log(result)              /* отображаем в консоле отсортированый массив */
// }
// sort()                                   /* вызываем сортировку + отображение в консоле */     
// humans[0].showSomethink()                /* достаём функцию Єкземпляра */
// Human.showSomethink2()                     /* достаём метод функции-конструктора */
// Human.prototype.showSomethink3()            /* достаём меиод с прототипа */